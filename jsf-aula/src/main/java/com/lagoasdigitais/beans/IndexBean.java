package com.lagoasdigitais.beans;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class IndexBean {

	private String nome;
	private int idade;
	
	private String resultado;

	public String getNome() {
		return nome;
	}

	public void setNome( String nome ) {
		this.nome = nome;
	}
	
	public int getIdade() {
		return idade;
	}

	public void setIdade( int idade ) {
		this.idade = idade;
	}
	
	public String getResultado() {
		return resultado;
	}

	public void setResultado( String resultado ) {
		this.resultado = resultado;
	}

	public void enviar() {
		this.resultado = this.nome + this.idade;
	}

}
