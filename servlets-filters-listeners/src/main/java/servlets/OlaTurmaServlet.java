package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/olaturma")
public class OlaTurmaServlet extends HttpServlet {

	private static final long serialVersionUID = 5570629290325353174L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		if (req.getParameter("aluno").equals("antonio")) {
			resp.getWriter().write("olá antonio!!");
		} else
			resp.getWriter().write("saia seu intruso");

	}

}
