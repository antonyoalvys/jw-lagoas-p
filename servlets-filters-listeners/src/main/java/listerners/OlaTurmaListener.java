package listerners;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class OlaTurmaListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		System.out.println("request sendo encerrado");
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		System.out.println("request sendo enviado");
	}

}
