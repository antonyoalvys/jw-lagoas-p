package com.lagoasdigitais.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class IndexController {

	private String mensagem;

	public void enviar() {
		SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy hh:mm:ss" );
		Date agora = new Date();
		this.mensagem = String.format( "Você enviou a mensagem às %s \n\n --> %s", sdf.format( agora ), this.mensagem );

	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem( String mensagem ) {
		this.mensagem = mensagem;
	}

}
