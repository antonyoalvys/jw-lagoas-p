package com.lagoasdigitais.livraria.domain.entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table( name = "endereco" )
public class Endereco implements EntidadeBase<Long> {

	private static final long serialVersionUID = 3782691049645879813L;

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@Size( max = 100 )
	private String cidade;

	@NotNull
	@Size( max = 100 )
	private String uf;

	@Size( max = 100 )
	private String logradouro;

	private Integer numero;

	@Size( max = 100 )
	private String bairro;

	@Size( max = 9 )
	private String cep;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade( String cidade ) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf( String uf ) {
		this.uf = uf;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro( String logradouro ) {
		this.logradouro = logradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero( Integer numero ) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro( String bairro ) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep( String cep ) {
		this.cep = cep;
	}

}
