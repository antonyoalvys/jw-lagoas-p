package com.lagoasdigitais.livraria.domain.repositorio.impl;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.persistence.Query;

import com.lagoasdigitais.livraria.domain.entidades.Autor;
import com.lagoasdigitais.livraria.domain.repositorio.AutorRepositorio;

@Named
@Dependent
public class AutorRepositorioImpl extends RepositorioBaseImpl<Autor> implements AutorRepositorio {

	@Override
	public Class<Autor> getClassT() {
		return Autor.class;
	}

	@SuppressWarnings( "unchecked" )
	@Override
	public List<Autor> findByFields( String nome, int maxResults, String orderBy ) {

		StringBuilder strbld = new StringBuilder( "from " + getClassT().getName() + " t" );
		String connector = " where ";

		if ( nome != null && !nome.isEmpty() ) {
			strbld.append( connector + "lower(t.nome) " + "like '%'||lower(:nome)||'%'" );
		}

		if ( orderBy != null ) {
			if ( !"".equals( orderBy.trim() ) )
				strbld.append( " order by t." + orderBy );
		}

		Query query = em.createQuery( strbld.toString() );
		if ( maxResults > 0 )
			query.setMaxResults( maxResults );

		if ( nome != null && !nome.isEmpty() ) {
			query.setParameter( "nome", nome );
		}

		return (List<Autor>) query.getResultList();
	}

}
