package com.lagoasdigitais.livraria.domain.repositorio;

import java.util.List;

import com.lagoasdigitais.livraria.domain.entidades.Autor;

public interface AutorRepositorio extends RepositorioBase<Autor> {
	
	public List<Autor> findByFields( String nome, int maxResults, String orderBy );

}
