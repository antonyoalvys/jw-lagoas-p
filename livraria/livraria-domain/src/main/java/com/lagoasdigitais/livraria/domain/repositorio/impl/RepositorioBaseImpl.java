package com.lagoasdigitais.livraria.domain.repositorio.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import com.lagoasdigitais.livraria.domain.entidades.EntidadeBase;
import com.lagoasdigitais.livraria.domain.repositorio.RepositorioBase;

public abstract class RepositorioBaseImpl<T extends EntidadeBase<?>> implements
		RepositorioBase<T> {

	@PersistenceContext
	protected EntityManager em;
	
	@Override
	@Transactional
	public void save(T entity) {
		Object id = entity.getId();

		if (id == null)
			em.persist(entity);
		else
			em.merge(entity);

	}

	@Override
	@Transactional
	public void delete(Object id) throws EntityNotFoundException {
		em.remove(this.getById(id));
	}

	@Override
	public T getById(Object id) throws EntityNotFoundException {
		T entity = em.find(getClassT(), id);

		if (entity == null)
			throw new EntityNotFoundException();
		else
			return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() {
		Query query = em.createQuery("from " + getClassT().getName() + " t");
		return query.getResultList();
	}

	public abstract Class<T> getClassT();
	
}
