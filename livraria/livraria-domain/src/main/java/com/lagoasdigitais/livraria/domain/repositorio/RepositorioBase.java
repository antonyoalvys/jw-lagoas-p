package com.lagoasdigitais.livraria.domain.repositorio;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import com.lagoasdigitais.livraria.domain.entidades.EntidadeBase;

public interface RepositorioBase<T extends EntidadeBase<?>> {

	void save( T entity );

	void delete( Object id ) throws EntityNotFoundException;

	T getById( Object id ) throws EntityNotFoundException;

	List<T> findAll();

}
