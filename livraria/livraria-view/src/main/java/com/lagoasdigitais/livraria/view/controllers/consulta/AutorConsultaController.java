package com.lagoasdigitais.livraria.view.controllers.consulta;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.lagoasdigitais.livraria.domain.entidades.Autor;
import com.lagoasdigitais.livraria.service.AutorService;

@Named
@ViewScoped
public class AutorConsultaController implements Serializable {

	private static final long serialVersionUID = 1118391497664816871L;

	@Inject
	private AutorService autorService;

	private List<Autor> autorList;
	private Autor autorVisualizar;
	private Autor autorEditar;
	private Autor autor;
	private Autor autorRemover;

	@PostConstruct
	private void init() {
		limpar();
	}

	public void consultar() {
		this.autorList = autorService.finByFields( autor.getNome(), 100, "nome" );
	}

	public void remover() {
		autorService.remove( autorRemover.getId() );
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_INFO, "O autor " + autorRemover.getNome() + " foi removido com sucesso.", null ) );
		autorRemover = new Autor();
		consultar();
	}

	public void show( Autor autorShow ) {
		this.autorVisualizar = autorShow;
	}

	public void showEditar( Autor autorEditar ) {
		this.autorEditar = autorEditar;
	}

	public void salvar() {
		autorService.save( autorEditar );
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_INFO, "Autor alterado com sucesso.", null ) );
		consultar();
	}

	public void limpar() {
		this.autor = new Autor();
		consultar();
	}

	public Autor getAutorVisualizar() {
		return autorVisualizar;
	}

	public void setAutorVisualizar( Autor autorVisualizar ) {
		this.autorVisualizar = autorVisualizar;
	}

	public Autor getAutorEditar() {
		return autorEditar;
	}

	public void setAutorEditar( Autor autorEditar ) {
		this.autorEditar = autorEditar;
	}

	public List<Autor> getAutorList() {
		return autorList;
	}

	public void setAutorList( List<Autor> autorList ) {
		this.autorList = autorList;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor( Autor autor ) {
		this.autor = autor;
	}

	public Autor getAutorRemover() {
		return autorRemover;
	}

	public void setAutorRemover( Autor autorRemover ) {
		this.autorRemover = autorRemover;
	}
	
}
