package com.lagoasdigitais.livraria.view.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.lagoasdigitais.livraria.domain.entidades.Autor;
import com.lagoasdigitais.livraria.domain.entidades.Endereco;
import com.lagoasdigitais.livraria.service.AutorService;

@Named
@ViewScoped
public class AutorController implements Serializable {

	private static final long serialVersionUID = 6680239599309762240L;

	@Inject
	private AutorService autorService;

	private Autor autor;

	private List<Autor> autorList = new ArrayList<Autor>();

	@PostConstruct
	public void init() {
		this.autor = new Autor();
		this.autor.setEndereco( new Endereco() );
	}

	public void salvar() {
		autorService.save( autor );
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_INFO, "Autor criado com sucesso.", null ) );
		limparEndereco();
	}

	public void limparEndereco() {
		this.autor = new Autor();
		this.autor.setEndereco( new Endereco() );
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor( Autor autor ) {
		this.autor = autor;
	}

	public List<Autor> getAutorList() {
		return autorList;
	}

	public void setAutorList( List<Autor> autorList ) {
		this.autorList = autorList;
	}

}
