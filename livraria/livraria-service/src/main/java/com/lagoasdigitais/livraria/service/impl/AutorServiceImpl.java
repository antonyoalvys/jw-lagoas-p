package com.lagoasdigitais.livraria.service.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.lagoasdigitais.livraria.domain.entidades.Autor;
import com.lagoasdigitais.livraria.domain.repositorio.AutorRepositorio;
import com.lagoasdigitais.livraria.service.AutorService;

@Stateless
public class AutorServiceImpl implements AutorService {

	@Inject
	private AutorRepositorio autorRepositorio;

	@Override
	public void save( Autor entidade ) {
		autorRepositorio.save( entidade );
	}

	@Override
	@TransactionAttribute( TransactionAttributeType.REQUIRED )
	public void remove( Long id ) {
		autorRepositorio.delete( id );
	}

	@Override
	@TransactionAttribute( TransactionAttributeType.SUPPORTS )
	public List<Autor> finByFields( String nome, int maxresults, String orderBy ) {
		return autorRepositorio.findByFields( nome, maxresults, orderBy );
	}

	@Override
	@TransactionAttribute( TransactionAttributeType.SUPPORTS )
	public List<Autor> findAll() {
		return autorRepositorio.findAll();
	}

	@Override
	public Autor load( Long id ) {
		return autorRepositorio.getById( id );
	}

}
