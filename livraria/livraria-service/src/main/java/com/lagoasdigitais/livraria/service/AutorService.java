package com.lagoasdigitais.livraria.service;

import java.util.List;

import com.lagoasdigitais.livraria.domain.entidades.Autor;

public interface AutorService {

	Autor load( Long id );

	void save( Autor entidade );

	void remove( Long id );

	List<Autor> finByFields( String nome, int maxresults, String orderBy );

	List<Autor> findAll();

}
